import Head from 'next/head';
import styles from '../styles/Home.module.css';
import {useState, useEffect} from 'react';
import Link from 'next/link';

export default function Home({posts}) {
  const [count, setCount] = useState (0);
  useEffect(() => {
    const timer = window.setInterval(() => setCount(n => n + 1), 1000)
    return () => {
      window.clearInterval(timer)
    }
  }, [])

  return (
    <>
      <Head>
        <title>Test API blog on Next js</title>
      </Head>

      <h1>Count: {count}</h1>

      <ul>
        {posts.map(post => <li>
        <Link href={`/blog/${post.id}`}>
          < a>
            <h3>{post.id} - {post.title}</h3>
          </a>
        </Link>
        </li>
        )}
        
      </ul>
    </>
  )
}

// maintenant, j'utilise la fonction "getStaticProps" pr que le contenu de la page soit généré en amont, et que le user n'ai pas à attendre le chargement de la page:
/**
 * getStaticProps
 * 
 * ma liste est directement généré dans le HTML (on pt verifier avec code source, on y voit aussi l'étape de réconciliation avec React qui doit avoir 
 * accès aux données, React prend alors le relais)
 * donc: Next récupère les données en amont, puis donne le rendu ! 
 */

// USE SSG:
// export async function getStaticProps() {

//   const posts = await fetch('https://jsonplaceholder.typicode.com/posts?_limit=6')
//   .then(r => r.json())
  
//     return {
//       props: {
//         posts
//       }
//     }
// }

// USE SSR:
export async function getServerSideProps () {

  const posts = await fetch('https://jsonplaceholder.typicode.com/posts?_limit=6')
  .then(r => r.json())
  
    return {
      props: {
        posts
      }
    }
}
