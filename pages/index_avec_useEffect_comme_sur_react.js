import Head from 'next/head';
import styles from '../styles/Home.module.css';
import {useState, useEffect} from 'react';

export default function Home() {

  const [posts, setPosts] = useState([])

  useEffect(() => {

    const data = fetch('https://jsonplaceholder.typicode.com/posts?_limit=6')
    .then(r => r.json())
    .then(setPosts)       
  }, [])

  return (
    <>
      <Head>
        <title>Test API blog on Next js</title>
      </Head>

      <ul>
        {posts.map(post => <li>
          <h3>{post.title}</h3>
        </li>)}
        
      </ul>
    </>
  )
}
